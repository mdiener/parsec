#!/bin/bash

set -o errexit; set -o nounset

if [ $# -ne 3 ]; then
        echo "Usage: $0 <app> <input> <nthreads>"; exit 1
fi

app="$1"
input="$2"
NTHREADS="$3"

if [ -d "./pkgs/apps/$app/" ]; then
	kernelpp="apps"
elif [ -d "./pkgs/kernels/$app/" ]; then
	kernelpp="kernels"
else
	echo "Bad app/kernel $app"
	exit
fi

mkdir -p my-parsec/$app/$input

if [ -f "./pkgs/$kernelpp/$app/inputs/input_$input.tar" ]; then
	cd my-parsec/$app/$input
	ifile="../../../pkgs/$kernelpp/$app/inputs/input_$input.tar"
	tar -k --keep-old-files -xf $ifile
	cd ../../../
fi

cfg=`ls ./pkgs/$kernelpp/$app/inst`
cd my-parsec/$app/$input

. ../../../pkgs/$kernelpp/$app/parsec/$input.runconf

cmd="../../../pkgs/$kernelpp/$app/inst/$cfg/$run_exec.x $run_args"

echo "$cmd"
#LD_PRELOAD=~/Work/libmapping/libmapping.so ./$cmd
./$cmd
