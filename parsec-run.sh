#!/bin/bash

# set -o errexit

if [ $# -ne 2 ]; then
	echo "Usage: $0 <benchmark> <nthreads>"; exit 2
fi

app="$1"
input="native"
nt="$2"

case $nt in
	32)
		case $app in
			bodytrack)
				NTHREADS=30;;
			facesim|freqmine|fluidanimate)
				NTHREADS=32;;
			ferret|dedup|streamcluster)
				NTHREADS=8;;
			vips)
				NTHREADS=29;;
			*)
				NTHREADS=31;;
		esac
		;;
	64)
		case $app in
			bodytrack)
				NTHREADS=62;;
			facesim|freqmine|fluidanimate)
				NTHREADS=64;;
			ferret|dedup|streamcluster)
				NTHREADS=16;;
			vips)
				NTHREADS=61;;
			*)
				NTHREADS=63;;
		esac
		;;
	*)
		NTHREADS=$nt
		echo "Warning: unknown number of threads $nt"
		;;
esac

if [ -d "./pkgs/apps/$app/" ]; then
	kernelpp="apps"
elif [ -d "./pkgs/kernels/$app/" ]; then
	kernelpp="kernels"
else
	echo "Unknown benchmark $app"
	exit 1
fi

mkdir -p my-parsec/$app/$input
cd my-parsec/$app/$input

if [ -f "../../../pkgs/$kernelpp/$app/inputs/input_$input.tar" ]; then
	tar -k --keep-old-files -xf "../../../pkgs/$kernelpp/$app/inputs/input_$input.tar"
fi

cfg=$(ls ../../../pkgs/$kernelpp/$app/inst)

source ../../../pkgs/$kernelpp/$app/parsec/$input.runconf

cmd="../../../pkgs/$kernelpp/$app/inst/$cfg/$run_exec $run_args"

echo "$cmd"

# export LIBMAPPING_PLACE=near
# export LIBMAPPING_PLACE=0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63

# NUMA="numactl -i all"

# LD_PRELOAD=~/Work/libmapping/libmapping.so $NUMA $cmd
# time -p $NUMA $cmd
# export LD_PRELOAD=~/Work/libmapping/libmapping.so

~/Work/numalize/run.sh -- $cmd
unset LD_PRELOAD
# sleep 0.5

# rm -f ~/Work/parsec/power_info_3.txt

# pidof rtview > /dev/null

# while [ $? -eq 0 ]
# do
# 	date >> ~/Work/parsec/power_info_3.txt
# 	sudo ipmi-oem Dell get-instantaneous-power-consumption-info >> ~/Work/parsec/power_info_3.txt
# 	sleep 0.2s
# 	pidof rtview > /dev/null
# done
